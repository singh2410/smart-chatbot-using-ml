#!/usr/bin/env python
# coding: utf-8

# # Smart Chatbot using ML
# #By- Aarush Kumar
# #Dated:May 21,2021

# In[2]:


get_ipython().system('pip install nltk')
get_ipython().system('pip install newspaper3k')


# In[1]:


from newspaper import Article
import random
import string
import nltk
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import warnings
warnings.filterwarnings('ignore')


# In[2]:


nltk.download('punkt',quiet=True)


# In[3]:


article=Article('https://www.mayoclinic.org/diseases-conditions/chronic-kidney-disease/symptoms-causes/syc-20354521')
article.download()
article.parse()
article.nlp()
corpus=article.text


# In[4]:


print(corpus)


# In[5]:


#Tokenization
text = corpus
sentence_list=nltk.sent_tokenize(text)
print(sentence_list)


# In[6]:


# function to return greeting response
def greeting_response(text):
    text=text.lower()
    bot_greetings=['howdy','hola','hi','hello','hey','namaste']
    user_greetings=['hi','hey','hello','howdy','namaste','hola','greetings','wassup']
    for word in text.split():
        if word in user_greetings:
            return random.choice(bot_greetings)


# In[7]:


def index_sort(list_var):
    length=len(list_var)
    list_index=list(range(0,length))
    x=list_var
    for i in range(length):
        for j in range(length):
            if x[list_index[1]]>x[list_index[j]]:
                #swap
                temp=list_index[i]
                list_index[i]=list_index[j]
                list_index[j]=temp
    return list_index


# In[8]:


#bot response
def bot_response(user_input):
    user_input=user_input.lower()
    sentence_list.append(user_input)
    bot_response=''
    cm=CountVectorizer().fit_transform(sentence_list)
    similarity_scores=cosine_similarity(cm[-1],cm)
    similarity_scores_list=similarity_score.flatten()
    index=index_sort(similarity_scores_list)
    index=index[1:]
    response_flag=0
    j=0
    for i in range(len(index)):
        if similarity_scores_list[index[i]]>0.0:
            bot_response=bot_response+' '+sentence_list[index[1]]
            response_flag=1
            j=j+1
            if j>2:
                break
            if response_flag==0:
                bot_response=bot_response+' '+"Sorry,I don't unserstand."
                sentence_list.remove(user_input)
                return bot_response


# In[15]:


# start bot
print('Bot: I am a Doctor bot.I am designed to anwer your queries regarding Chronic Kidney Disease.If you want to exit,type bye')
exit_list=['exit','see you later','bye']
while(True):
    user_input=input()
    if user_input.lower()in exit_list:
        print('Bot: Chat with you later!')
        break
    else:
        if greeting_response(user_input) !=None:
            print('Bot: '+greeting_response(user_input))
        else:
            print('Bot: '+bot_response(user_input))

