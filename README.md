# Smart Chatbot Using ML
# By- Aarush Kumar
Built a chatbot which using ML to answer the question of user on Chronic Kidney Disease.
A chat bot is software that conducts conversations. Many chat bots are created to simulate how a human would behave as a conversational partner. Chat bots are in many devices, for example Siri, Cortona, Alexa, and Google Assistant. Many chat bots are used now a days for customer service.
There are broadly two variants of chat bots: Rule-Based and Self Learning.
A Rule-Based chat bot is a bot that answers questions based on some rules that it is trained on, while a Self Learning chat bot is a chat bot that uses some Machine Learning based technique to chat.
Used: NLP,BOW.
Thankyou!
